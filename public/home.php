		<div class="row">
			<div class="col-md-8">
				<div class="panel panel-default">
					<div class="panel-heading">
						<h3 class="panel-title"><i class="fa fa-graduation-cap" aria-hidden="true"></i>&nbsp;<?php echo 'Bienvenidos al simulador de destinos AGE' ?></h3>
					</div>
					<div class="panel-body">
						<?php
							echo 'Bienvenidos al simulador de destinos para Aprobados de la oposición de Administrativo del Estado en base a la resolución del 28 de mayo de 2020. Sólo aquellos opositores con usuario y contraseña pueden introducir sus petición de destinos. Si no tienes usuario o password no dudes en registrarte y seguir los pasos que recibirás en tu correo electrónico.';
							
						?>
					</div>
				</div>
			</div>



			<div class="col-md-4">
				<div class="panel panel-default">
					<div class="panel-heading">
						<h3 class="panel-title"><i class="fa fa-bar-chart" aria-hidden="true"></i>&nbsp;<?php echo 'Estadísticas'; ?></h3>
					</div>
					<div class="panel-body">
						<table class="table table-hover">
							<tr>
								<td><i class="fa fa-trophy fa-fw"></i> <?php echo 'Plazas totales'; ?></td>
								<td><?php echo $plazas; ?></td>
							</tr>
                        	<tr>
								<td><i class="fa fa-trophy fa-fw"></i> <?php echo 'Plazas Grupo 5'; ?></td>
								<td><?php echo $grupo5; ?></td>
							</tr>
							<tr>
								<td><i class="fa fa-users fa-fw"></i> <?php echo 'Opositores registrados'; ?></td>
								<td><?php echo $num_opositores; ?></td>
							</tr>
							<tr>
								<td><i class="fa fa-globe fa-fw"></i> <?php echo 'Destinos'; ?></td>
								<td><?php echo $num_destinos; ?></td>
							</tr>
							<tr>
								<td><i class="fa fa-code-fork fa-fw"></i> <?php echo 'Elecciones'; ?></td>
								<td><?php echo $num_peticiones; ?></td>
							</tr>
							<tr>
								<td><i class="fa fa-bar-chart fa-fw"></i> <?php echo '% Opositores'; ?></td>
								<td><?php   
										echo round($perc_plazas, 2).' %';
									 ?>
								</td>
							</tr>
						</table>
					</div>
				</div>
				<div class="clearfix visible-lg"></div>
			</div>
		</div>




		<div class="row">
			<div class="col-md-8">
				<div class="panel panel-default">
					<div class="panel-heading">
						<h3 class="panel-title"><i class="fa fa-graduation-cap" aria-hidden="true"></i>&nbsp;<?php echo 'Web en prueba, con datos de prueba' ?></h3>
					</div>
					<div class="panel-body">
						<li>Los datos cargados son de prueba, los destinos son los del año pasado pero repetidos hasta alcanzar los 2029 destinos.
						<br>
						<li>Los datos de usuarios son de prueba, actualmente cargados los 2029 opositores. Cuando se publiquen los destinos reales se crearán los usuarios reales o se abrirá e registro.
						<br>
						<li>Usuarios para pruebas desde el 1001 al 3029, podeis probar con cualquier usuario. Ejemplo:
						<br> - Usuario 1001 con contraseña 1001 ,que es el opositor en posición 1
						<br> - Usuario 1002 con contraseña 1002 ,que es el opositor en posición 2
						<br> - Usuario 1003 con contraseña 1003 ,que es el opositor en posición 3
						<br> Así sucesivamente hasta el usuario 3029 con contraseña 3029 y posición 2029
						<li> Por favor visualizar el vídeo demo que hice para saber como usar la web y solicitar destinos. Podeís probar lo que queráis. En caso de error escribir por Telegram mencionándome o un mail a dirección de abajo indicando el usuario que tiene el error.
						<br>
						<li> <a href="https://www.youtube.com/watch?v=N-UaXEahBvU">Vídeo demo en Youtube</a>
					</div>
				</div>
			</div>

			<div class="col-md-4">
				<div class="panel panel-danger">
					<div class="panel-heading">
						<h3 class="panel-title"><i class="fa fa-bar-chart" aria-hidden="true"></i>&nbsp;<?php echo 'Nota sobre navegadores'; ?></h3>
					</div>
					<div class="panel-body">
						<li>Navegador recomendado Firefox.
						<br>
						<li>Si usas Chrome hazlo en modo incógnito. Intentaré solucionar un problema que hay con Chrome en modo no de incógnito.
						<br>

					</div>
				</div>
				<div class="clearfix visible-lg"></div>
			</div>

		</div>



		</div>
		<br>
		<!-- HOME PAGE End -->