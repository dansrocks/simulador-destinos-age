
<div id="no_search_result">
  <div class="row">
    <div class="col-md-12">
      <div class="panel panel-default">
        <!-- Default panel contents -->
        <div class="panel-heading"><IMG src="images/icons/ic_public_white_18dp_1x.png">&nbsp;<?php echo 'Cambios en la web' ?></div>
        <div class="table-responsive">
        <br>
        <!-- Table -->
        <table id="Release notes" class="table table-hover">
          <tr>
            <td style="text-align:center; vertical-align:middle"><b>Versión 0.7</b></td>
            <td>
              <li>Añadido el códido del destino en resultado de la asignación.</li>
              <li>Añadido la lista de cambios para cada nueva versión.</li>
              <li>Renombrado Duplicados por Repetidos.</li>
              <li>Nueva página que muestra alertas al usuario sobre sus peticiones de destino e informa sobre si otros usuarios por delante han solicitado destinos.</li>
              <li>Nueva página que muestra listado de posiciones sin solicitar.</li>
              <li>Control para evitar que un usuario introduzca mas destinos de su posicion + Cupo reserva personas con discapacidad.</li>
            </td>
          <tr>

          <tr>
            <td style="text-align:center; vertical-align:middle"><b>Versión 0.8</b></td>
            <td>
              <li>Número de casillas disponibles se muestran en base a posición + Cupo reserva personas con discapacidad.</li>
            </td>
          <tr>

          <tr>
            <td style="text-align:center; vertical-align:middle"><b>Versión 0.9</b></td>
            <td>
              <li>Cuando usuario se ha logado la página principal muestra la información del usuario. Se elimina el link mis alertas , ya que ahora esta en la pagna principal.</li>
              <li>Los usuarios pueden solicitar los destinos a traves de la carga de un fichero excel.</li>
            </td>
          <tr>   

          <tr>
            <td style="text-align:center; vertical-align:middle"><b>Versión 1.0</b></td>
            <td>
              <li>El sistema envia un email a los administradores cuando hay un nuevo usuario registrado.</li>
              <li>El sistema envia un email a usuario registrado confirmando su activación.</li>
              <li>Mejoras internas en manejo de ficheos excel cargados por los usuarios.</li>
              <li>El sistema avisa al usario sobre que fichero excel fue cargado por última vez.</li>
              <li>Administradores pueden inactivar a usuarios desde la administración.</li>
              <li>El usuario puede recuperar la contraseña si no la recuerda.</li>
              <li>El usuario puede exportar un excel con toda la información de los destinos solicitados.</li>
            </td>
          <tr>   

          <tr>
            <td style="text-align:center; vertical-align:middle"><b>Versión 1.1</b></td>
            <td>
              <li>Bugfix en casillas a mostrar en selección de destinos.</li>
              <li>Mejoras internas para menor consumo de recursos en el servidor.</li>
            </td>
          <tr>        
        </table>
      </div>
    </div>
  </div>
    <div class="clearfix visible-lg"></div>
  </div>
</div>

