<?php
	session_start();
	include('db_login.php');
	$db = new mysqli($servername , $db_username , $db_password , $db_database);
	$db->set_charset("utf8");
	if ($db->connect_errno > 0) {
		die('Unable to connect to database [' . $db->connect_error . ']');
	}
	$exists = 0;
	$to_secure = 0;
	$_SESSION["access"] = false;
	$_SESSION["es_admin"] = false;
	if (isset($_POST['user']) and isset($_POST['password'])) {
		$user = mysqli_real_escape_string($db , $_POST['user']);
		$Encrypt_Pass = md5(mysqli_real_escape_string($db , $_POST["password"]));
		$sql = "SELECT * FROM opositor where activo=1 and dni=:para1";
		$result=get_data ($sql,$user,null);

		foreach ($result as $row) {
			$hash = $row['new_password'];
			if (password_verify ($_POST["password"],$hash)==1)
			{
				$exists=1;
				$dni = $row['Dni'];
				$nombre = $row['Nombre'];
				$apellidos = $row['Apellidos'];
				$correo = $row['Correo'];
				$id = $row['id'];
				$es_admin = $row['es_admin'];
				$prelacion = $row['prelacion'];

			}
			
		}
		if ($exists != 0) {
			$id = $row['id'];
			$_SESSION["access"] = true;
			$_SESSION["Dni"] = $dni;
			$_SESSION["Nombre"] = $nombre;
			$_SESSION["Apellidos"] = $apellidos;
			$_SESSION["Correo"] = $correo;
			$_SESSION["id"] = $id;
			$_SESSION["es_admin"] = $es_admin;
			$_SESSION["prelacion"] = $prelacion;

			//$lifetime = 3600;
			//session_set_cookie_params($lifetime);

			// update last visit
			$sql = "UPDATE opositor set ultimo_login=now() where Dni=:para1";
			$result=get_data ($sql,$user,null);


			echo "<strong>Accediendo al Simulador Destinos AGE  " . $_SESSION["Nombre"] . "</strong>";

			echo "<META HTTP-EQUIV='Refresh' CONTENT='0; URL=./index.php'>";
			exit;
		} else {
			$db->close();
			include("./notgranted.php");
		}
	}
?>