<?php
  require 'vendor/autoload.php';

  use PhpOffice\PhpSpreadsheet\Spreadsheet;
  use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
  use PhpOffice\PhpSpreadsheet\IOFactory;
if ($user_logged== 1){

  //session_start();
  $maximo_pos= $prelacion + $grupo5 ;

  if ($maximo_pos>$plazas-$grupo5){
    $maximo_pos= $prelacion + ($plazas -$prelacion);
  }
  else{
    $maximo_pos=$grupo5 + $prelacion;
  }


  
  $opositor=$_SESSION['Dni'];
  $prelacion = $_SESSION['prelacion'];
    $currentDirectory = getcwd();
    $uploadDirectory = "/excelin/";

    $errors = []; // Store errors here

    $fileExtensionsAllowed = ['xls','xlsx']; // These will be the only file extensions allowed 

    $fileName = $_FILES['the_file']['name'];
    $fileSize = $_FILES['the_file']['size'];
    $fileTmpName  = $_FILES['the_file']['tmp_name'];
    $fileType = $_FILES['the_file']['type'];
    $fileExtension = strtolower(end(explode('.',$fileName)));

    $uploadPath = $currentDirectory . $uploadDirectory .$opositor .'_'.basename($fileName); 

    //echo '$uploadPath:'.$uploadPath;

    if (isset($_POST['submit'])) {

      if (! in_array($fileExtension,$fileExtensionsAllowed)) {
        $errors[] = "Este fichero no es formato Excel. Por favor cargue sólo ficheros en formato Excel";
      }

      if ($fileSize > 2000000) {
        $errors[] = "El fichero es demasiado grande (2MB)";
      }

      if (empty($errors)) {
        $didUpload = move_uploaded_file($fileTmpName, $uploadPath);

        if ($didUpload) {
          // logica de caga del fichero en la bbdd COMIENZO


            // borro todas las peticiones para este user

                  // borro las peticiones anteriores de este opositor para esta posicion
        
                  // Create connection
                  $conn = new mysqli($servername, $db_username, $db_password, $db_database);
                  // Check connection
                  if ($conn->connect_error) {
                    die("Connection failed: " . $conn->connect_error);
                  }
                  $sql = "DELETE FROM Peticiones WHERE opositor=$opositor";
                  if ($conn->query($sql) === TRUE) {
                    echo "";
                  } else {
                    echo "Error deleting record: " . $conn->error;
                  }

            // inserto desde el excel en la tabla Peticiones

            $inputFileType = 'Xlsx';
            $inputFileName = './excelin/'.$opositor .'_'.basename($fileName);
            //$inputFileName =$uploadPath
            $reader = IOFactory::createReader('Xlsx');
            $spreadsheet = $reader->load($inputFileName);
            $sheetData = $spreadsheet->getActiveSheet()->toArray(null, true, true, true);
            //var_dump($sheetData);
            for ($i=1;$i<=$maximo_pos;$i++){
              if ($sheetData[$i][A]!="" && $sheetData[$i][B]!=""){
                $data_name = Array('opositor','posicion', 'destino','prelacion');
                $data_para = Array($opositor,$sheetData[$i][A],$sheetData[$i][B],$prelacion);
                $table = 'Peticiones';
                ins_data($table,$data_name,$data_para); 
                //echo 'Posicion : '.$sheetData[$i][A] . ' destino '. $sheetData[$i][B];
              }
              
              //var_dump($sheetData[$i]);
            }

            // borrado posiciones que sobra

            
            $sql = "DELETE FROM Peticiones WHERE opositor=$opositor and posicion>$maximo_pos";
            //echo '<br>'.$sql;
            if ($conn->query($sql) === TRUE) {
                echo "";
              } else {
                echo "Error deleting record: " . $conn->error;
              }              
            $conn->close();

            // guardo en opositor info de su excel
            $sql = "update opositor set excel_name='".basename($fileName)."' ,excel_load = now() where Dni=$opositor";
            //echo $sql;
            $result = get_data ($sql,null,null);

          // logica de caga del fichero en la bbdd FINAL
          //echo "El fichero " . basename($fileName) . " ha sido cargado";
          echo "Carga completada, el sistema volverá a página principal en 30 segundos.<p> Si se han producido errores vuelve a intentarlo.<META HTTP-EQUIV='Refresh' CONTENT='30; URL=./index.php'>";
        } else {
          echo "Error, revise el formato de fichero, si el error persiste mande un correo al administrador.";
        }
      } else {
        foreach ($errors as $error) {
          echo $error . "" . "\n";
        }
      }

    }
  } // else de estar logado
  else{
    include("./notgranted.php");
  }
?>