<?php
session_start();
$es_admin=$_SESSION["es_admin"];
if ($es_admin== 1){
    $opositor=$_SESSION['Dni'];
      $sql = "select id,prelacion,Dni,Nombre,Apellidos,activo,es_admin from opositor order by prelacion asc ";
      $result = get_data ($sql,null,null);
    ?>
    <div id="no_search_result">
      <div class="row">
        <div class="col-md-12">
          <div class="panel panel-default">
            <!-- Default panel contents -->
            <div class="panel-heading"><IMG src="images/icons/ic_public_white_18dp_1x.png">&nbsp;<?php echo 'Listado de opositores' ?></div>
            <div class="table-responsive">
            <br>
            <!-- Table -->
            <table id="destinos" class="table table-hover">
              <?php
                echo '<thead>';
                echo '<tr><th>Orden</th><th>DNI</th><th>Nombre</th><th>Apellidos</th><th>Activo</th><th>Administrador</th><th>Activar</th><th>Inactivar</th></tr>';
                echo '</thead>';
                foreach ($result as $row) {      
                  echo '<tr>';
                  echo '<td>'.$row["prelacion"].'</td>' ;
                  echo '<td>'.$row["Dni"].'</td>' ;
                  echo '<td>'.$row["Nombre"].'</td>' ;
                  echo '<td>'.$row["Apellidos"].'</td>' ;
                  if ($row["activo"]==1)
                  {
                    echo '<td><img src="./images/green-user-icon.png" height="32" width="32"></td>';
                  }
                  else
                  {
                    echo '<td><img src="./images/red-user-icon.png" height="32" width="32"></td>';
                  }
                  if ($row["es_admin"]==1)
                  {
                    echo '<td><img src="./images/green-user-icon.png" height="32" width="32"></td>';
                  }
                  else
                  {
                    echo '<td><img src="./images/red-user-icon.png" height="32" width="32"></td>';
                  } 
                  echo '<td><a href="./index.php?page=activar&dni=' . $row["Dni"] . '">Activar</a></td>';
                  echo '<td><a href="./index.php?page=inactivar&dni=' . $row["Dni"] . '">Inactivar</a></td>';
                  echo '</tr>';
                }
              ?>
            </table>
          </div>
        </div>
      </div>
        <div class="clearfix visible-lg"></div>
      </div>
    </div>
<?php
}
  else
  {
    include("./notgranted.php");
  }
?>





